#!/usr/bin/env python
#
# This is a simple FTP server 
# 
# Copyright (C) 2014-2015 Near Technologies
# Author: Victor M. Varela

from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.log import logger

import os

def main():
    authorizer = DummyAuthorizer()
    ftp_users = os.getenv('FTP_USERS').split(',');
    for uservarname in ftp_users:
	ftp_username = os.getenv("FTP_"+uservarname+"_USERNAME",uservarname.lower())
	ftp_password = os.getenv("FTP_"+uservarname+"_PASSWORD",ftp_username)
        ftp_home = os.getenv("FTP_"+uservarname+"_HOME","/ftp/"+ftp_username)
	if not os.path.exists(ftp_home):
        	os.mkdir(ftp_home)
    	authorizer.add_user(ftp_username, ftp_password, ftp_home, perm='elradfmwM')

    handler = FTPHandler
    handler.authorizer = authorizer
    handler.banner = "Near FTP ready."
    handler.masquerade_address = os.getenv('FTP_IPADDRESS','0.0.0.0');
    handler.passive_ports = range(12020,12025)
    server = FTPServer(('', 21), handler)
    server.serve_forever()

if __name__ == "__main__":
    main()

